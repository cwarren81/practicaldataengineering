import requests 
import json 
import csv 
import datetime 
import pickle
from spotify_config import CLIENT_ID,CLIENT_SECRET


#### AUTHORIZATION CODE FLOW PART 1 ####



# url = 'https://accounts.spotify.com/authorize'

# request_body = {'client_id':CLIENT_ID,
# 				'response_type':'code',
# 				'redirect_uri':'http://retrix.io', ### Make sure you use the exact redirect URL you put in the 'Edit Settings' part of the app.
# 				'scope':'user-top-read user-library-read'}


# req = requests.get(url,params=request_body)
# print(req.url) ### In part 1, you will print the url and then paste it in a browser to get the code you will use in step 2


# raise SystemExit

#### END AUTHORIZATION CODE FLOW PART 1 ####

### AUTHORIZATION CODE FLOW PART 2 ###

#This is the code you got from part 1
# code = 'AQBJWy1LDIje0aKUoDdsUofq9b1doXSENpsUYODsa-yQ7aIJiFAhC7el_VYq7ijexBRbJNU-zcGRcgYUKS-yT-Ezo_8u0ekQn9s07_G7EMLT33W3UamDn7IGY6pEkAbQ0ZhhYG1faQk93AByUbvoq6WVJ8mF9qRlZzax-q0Gp8rpjtNcI1e3qe0A3XnuO_EP0cS_FTEa'

# url = 'https://accounts.spotify.com/api/token'

# request_body = {'client_id':CLIENT_ID,
# 				'client_secret':CLIENT_SECRET,
# 				'grant_type':'authorization_code',
# 				'code':code,
# 				'redirect_uri':'http://retrix.io'}


# req = requests.post(url,data=request_body)
# #print(req.text)
# response = req.json()
# print(response)

# refresh_token = response.get('refresh_token')
# access_token = response.get('access_token')

# filename = 'token.obj'
# with open(filename, 'wb') as f:
# 	pickle.dump(refresh_token, f)


#### END AUTHORIZATION CODE FLOW PART 2 ####

### AUTHORIZATION CODE FLOW PART 3 ###

# Get the refresh token from the pickle object 
filename = 'token.obj'
filehandler = open(filename, 'rb')
refresh_token = pickle.load(filehandler)

url = 'https://accounts.spotify.com/api/token'

request_body = {'client_id':CLIENT_ID,
				'client_secret':CLIENT_SECRET,
				'grant_type':'refresh_token',
				'refresh_token':refresh_token}


req = requests.post(url,data=request_body)
#print(req.text)
response = req.json()
#print(response)


access_token = response.get('access_token')



##### GET DATA TOP ARTISTS, TRACKS, and AUDIO FEATURES FROM THE SPOTIFY API USING THE ACCESS_TOKEN #####



# Get Your Top Artists ###
header = {'Authorization':f'Bearer {access_token}'}

url = 'https://api.spotify.com/v1/me/top/artists'

request_parameters = {'time_range':'long_term', # Use the paramaters from the documentation to adjust this
						'limit':'50'}


req = requests.get(url,params=request_parameters,headers=header)
response = req.json()


### Determine what keys are available to access

# print(response.keys())

# # keys: ['items', 'total', 'limit', 'offset', 'previous', 'href', 'next']


# ### What object (key) does the data reside in and how many records are there?

# print(len(response.get('items')))

# print(response.get('items'))

### How many records are there in total (for pagination...discussed later)

# print(response.get('total'))


# ### What "columns" are available for each record of items?

# for item in response.get('items'):
# 	print(item.keys())

# # Data objects we can access (columns for our dataset) : 'external_urls', 'followers', 'genres', 'href', 'id', 'images', 'name', 'popularity', 'type', 'uri'


# ### Organize the list data into 'rows and columns'

# for item in response.get('items'):
# 	row = [item.get('name'),
# 	item.get('id'),
# 	item.get('popularity'),
# 	item.get('type'),
# 	item.get('genres'),
# 	item.get('followers')
# 	]
# 	print(row)


# ### One of the items has nested JSON - how do we handle that?

# for item in response.get('items'):
# 	row = [item.get('name'),
# 	item.get('id'),
# 	item.get('popularity'),
# 	item.get('type'),
# 	item.get('genres'),
# 	item.get('followers').get('total') # You can add an additional '.get' to access the nested JSON
# 	]
# 	print(row)

# ### Create LIST OF LISTS (This will go nicely into a CSV)

artist_data = [] ## Create an empty list

for item in response.get('items'):
	row = [item.get('name'),
	item.get('id'),
	item.get('popularity'),
	item.get('type'),
	','.join(item.get('genres')),
	item.get('followers').get('total')
	]
	print(row)
	artist_data.append(row)

# print(artist_data)

# ### Write the list of lists into a csv 

write_file = 'artist_data.csv'
with open(write_file,'w', newline="") as f:
  writer = csv.writer(f)
  writer.writerows(artist_data)




# # ### Let's do the same thing for Tracks ###


header = {'Authorization':f'Bearer {access_token}'}

url = 'https://api.spotify.com/v1/me/top/tracks'

request_parameters = {'time_range':'long_term',
						'limit':'50'}


req = requests.get(url,params=request_parameters,headers=header)
#print(req.text)
response = req.json()

# #keys: ['items', 'total', 'limit', 'offset', 'previous', 'href', 'next']

# ### Determine what keys are available to access

# #print(response.keys())

# # keys: ['items', 'total', 'limit', 'offset', 'previous', 'href', 'next']


# ### What object (key) does the data reside in and how many records are there?

# #print(len(response.get('items')))
# #print(response.get('items'))


# ### What "columns" are available for each record of items?

# # for item in response.get('items'):
# # 	print(item.keys())
# # 	break


# # "Columns" = ['album', 'artists', 'available_markets', 'disc_number', 
# #'duration_ms', 'explicit', 'external_ids', 
# #'external_urls', 'href', 'id', 'is_local', 
# #'name', 'popularity', 'preview_url', 'track_number', 'type', 'uri']


# ### Put Tracks into List of Lists 

# # track_data = []
# # for item in response['items']:
# # 	row = [item.get('id'),
# # 	item.get('name'),
# # 	item.get('artists')[0].get('name'),
# # 	item.get('popularity'),
# # 	item.get('duration_ms')]
# # 	print(row)
# # 	track_data.append(row)


track_data = []
for item in response['items']:
	row = [item.get('id'),
	item.get('name'),
	item.get('artists'),
	item.get('popularity'),
	item.get('duration_ms')]
	#print(row)
	track_data.append(row)

# # # Put into a CSV File

write_file = 'track_data.csv'
with open(write_file,'w', newline="") as f:
  writer = csv.writer(f)
  writer.writerows(track_data)


# # #print(track_data)


# ##### GET AUDIO FEATURES FOR MY TOP TRACKS #####

### Get a comma seperate list of my top tracks
track_ids = []
for ids in track_data:
	track_ids.append(ids[0]) # Get the first element of every record, which contains the track ID


track_ids = ','.join(track_ids) # Join the IDS together into a comma seperated string 

header = {'Authorization':f'Bearer {access_token}'}

url = 'https://api.spotify.com/v1/audio-features'

request_parameters = {'ids':track_ids}

req = requests.get(url,params=request_parameters,headers=header)
#print(req.text)
response = req.json()

### What "columns" are available for each record of items?

for item in response.get('audio_features'):
	print(item.keys())
	break

#'danceability', 'energy', 'key', 'loudness', 'mode', 'speechiness', 'acousticness', 'instrumentalness', 'liveness', 'valence', 'tempo', 'type', 'id', 'uri', 'track_href', 'analysis_url', 'duration_ms', 'time_signature'

### Put Audio Features into List of Lists 
audio_features_data = []
for item in response['audio_features']:
	row = [item.get('id'),
	item.get('danceability'),
	item.get('energy'),
	item.get('key'),
	item.get('loudness'),
	item.get('mode'),
	item.get('speechiness'),
	item.get('acousticness'),
	item.get('instrumentalness'),
	item.get('liveness'),
	item.get('valence'),
	item.get('tempo')]
	audio_features_data.append(row)

print(audio_features_data)



### Write to CSV
write_file = 'audio_features.csv'
with open(write_file,'w', newline="") as f:
  writer = csv.writer(f)
  writer.writerows(audio_features_data)
print('Wrote data to csv...')












