from importdata import *
import csv




##### Push Artist Data #####

target_schema = 'public'
target_table = 'spotify_top_artists'
keys = ['id']

write_file = 'C:/Users/camw8/Practical Data Engineering/artist_data.csv'

data_to_db_from_csv(target_schema,target_table,keys,write_file,csv)



##### Push Track Data #####

target_schema = 'public'
target_table = 'spotify_top_tracks'
keys = ['id']

write_file = 'C:/Users/camw8/Practical Data Engineering/track_data.csv'

data_to_db_from_csv(target_schema,target_table,keys,write_file,csv)


##### Push Audio Features #####

target_schema = 'public'
target_table = 'spotify_audio_features'
keys = ['id']

write_file = 'C:/Users/camw8/Practical Data Engineering/audio_features.csv'

data_to_db_from_csv(target_schema,target_table,keys,write_file,csv)