import psycopg2
import datetime


HOST = '34.106.177.177'
DB = 'postgres'
USER = 'postgres'
PASSWORD = 'admin'
PORT = '5432'

conn = psycopg2.connect(host=HOST,database=DB,user=USER,password=PASSWORD,port=PORT)
 
# cursor = conn.cursor()
 
# this can bulk insert a file into a table
def create_temp_table(schema, table, temp_table):
	sql = '''
	DROP TABLE IF EXISTS {schema}.{temp_table};
	SELECT * INTO {schema}.{temp_table} FROM {schema}.{table} WHERE 1 = 0;
	COMMIT;
	'''.format(schema=schema,temp_table=temp_table,table=table)
	cursor = conn.cursor()
	cursor.execute(sql)
	cursor.close()
	del cursor

def copy_file_into(schema, table, file, delimiter=',',null='NULL',columns=[]):
	cursor = conn.cursor()
	fpath = open(file,'r')
	if len(columns) > 0:
		cols_str = ','.join(columns)
		copy_sql = '''COPY {schema}.{table} ({cols_str}) FROM STDIN WITH CSV DELIMITER E'{delimiter}' QUOTE '"' NULL '{null}';'''.format(schema=schema,table=table,cols_str=cols_str,delimiter=delimiter,null=null) # allows there to be embedded commas which in NICE!!
	else:
		copy_sql = '''COPY {schema}.{table} FROM STDIN WITH CSV DELIMITER E'{delimiter}' QUOTE '"' NULL '{null}';'''.format(schema=schema,table=table,delimiter=delimiter,null=null) # allows there to be embedded commas which in NICE!!
		# copy_sql = f'''COPY {schema}.{table} FROM STDIN WITH CSV DELIMITER E'\t' QUOTE '"' NULL 'NULL';''' # allows there to be embedded commas which in NICE!!
		# print(copy_sql)
		cursor.copy_expert(copy_sql, fpath)
		print('Done with the copy')
		cursor.execute('COMMIT;')
		cursor.close()
	del cursor
 
# use this to merge data from one table to another - you just need to define the sources, targets and primary key fields
def merge_one_table_with_main(source_schema,target_schema,source_table,target_table,list_of_unique_fields):
	# unique_where_filter_string = 'AND '.join(['{target_schema}.{target_table}.{col} = {source_schema}.{source_table}.{col}\n'.format(col=c,target_table=target_table,source_table=source_table,source_schema=source_schema,target_schema=target_schema) for c in list_of_unique_fields])
	unique_where_filter_string = 'AND '.join(['{target_schema}.{target_table}.{col} = {source_schema}.{source_table}.{col}\n'.format(target_schema=target_schema,target_table=target_table,col=col,source_schema=source_schema,source_table=source_table) for col in list_of_unique_fields])
	sql = '''
	delete from {target_schema}.{target_table}
	where exists (select 1 from {source_schema}.{source_table} where {unique_where_filter_string});
	insert into {target_schema}.{target_table} select * from {source_schema}.{source_table};
	drop table {source_schema}.{source_table};
	COMMIT;'''.format(target_schema=target_schema,target_table=target_table,source_schema=source_schema,source_table=source_table,unique_where_filter_string=unique_where_filter_string)
	# print(sql)
	cursor = conn.cursor()
	cursor.execute(sql)
	cursor.close()
	del cursor

def create_table(table_name, columns):
	cursor = conn.cursor()
	fields = ','.join(columns)
	sql = f'''
	CREATE TABLE IF NOT EXISTS {table_name} ({fields});
	COMMIT;
	'''
	cursor.execute(sql)
	print(f"Successfully created '{table_name}' table.")
	cursor.close()
	del cursor


#####################################
######## One Line Insert ###########
#####################################

def merge_data_to_db(listoflists,target_schema,target_table,keys,write_file,csv):
	print('Got '+ str(len(listoflists)) + ' records, time to write to the db....')
	with open(write_file,'w') as f:
	   writer = csv.writer(f)
	   writer.writerows(listoflists)


	create_temp_table(target_schema,target_table,target_table+'_tmp')
	print('created the temp table')

	copy_file_into(target_schema,target_table+'_tmp',write_file,delimiter=',')
	print('copied the data')

	merge_one_table_with_main(target_schema,target_schema,target_table+'_tmp',target_table,keys)
	print('merged temp table with main')

def data_to_db_from_csv(target_schema,target_table,keys,write_file,csv):

	create_temp_table(target_schema,target_table,target_table+'_tmp')
	print('created the temp table')

	copy_file_into(target_schema,target_table+'_tmp',write_file,delimiter=',')
	print('copied the data')

	merge_one_table_with_main(target_schema,target_schema,target_table+'_tmp',target_table,keys)
	print('merged temp table with main')