import requests 
import json 
import csv 
import datetime 
import pickle
from spotify_config import CLIENT_ID,CLIENT_SECRET

###### PAGINATION EXAMPLE ########


#### AUTHORIZATION CODE FLOW PART 1 ####

url = 'https://accounts.spotify.com/authorize'

request_body = {'client_id':CLIENT_ID,
				'response_type':'code',
				'redirect_uri':'http://retrix.io', ### Make sure you use the exact redirect URL you put in the 'Edit Settings' part of the app.
				'scope':'user-top-read user-library-read'} ### Add the user-library-read scope


req = requests.get(url,params=request_body)
print(req.url) ### In part 1, you will print the url and then paste it in a browser to get the code you will use in step 2


raise SystemExit

### END AUTHORIZATION CODE FLOW PART 1 ####

## AUTHORIZATION CODE FLOW PART 2 ###

This is the code you got from part 1
code = 'AQBJWy1LDIje0aKUoDdsUofq9b1doXSENpsUYODsa-yQ7aIJiFAhC7el_VYq7ijexBRbJNU-zcGRcgYUKS-yT-Ezo_8u0ekQn9s07_G7EMLT33W3UamDn7IGY6pEkAbQ0ZhhYG1faQk93AByUbvoq6WVJ8mF9qRlZzax-q0Gp8rpjtNcI1e3qe0A3XnuO_EP0cS_FTEa'

url = 'https://accounts.spotify.com/api/token'

request_body = {'client_id':CLIENT_ID,
				'client_secret':CLIENT_SECRET,
				'grant_type':'authorization_code',
				'code':code,
				'redirect_uri':'http://retrix.io'}


req = requests.post(url,data=request_body)
#print(req.text)
response = req.json()
print(response)

refresh_token = response.get('refresh_token')
access_token = response.get('access_token')

filename = 'token.obj'
with open(filename, 'wb') as f:
	pickle.dump(refresh_token, f)


### END AUTHORIZATION CODE FLOW PART 2 ####

## AUTHORIZATION CODE FLOW PART 3 ###

# Get the refresh token from the pickle object 
filename = 'token.obj'
filehandler = open(filename, 'rb')
refresh_token = pickle.load(filehandler)

url = 'https://accounts.spotify.com/api/token'

request_body = {'client_id':CLIENT_ID,
				'client_secret':CLIENT_SECRET,
				'grant_type':'refresh_token',
				'refresh_token':refresh_token}


req = requests.post(url,data=request_body)
#print(req.text)
response = req.json()
#print(response)


access_token = response.get('access_token')





### Pagination Example using the Library API (tracks endpoint)


header = {'Authorization':f'Bearer {access_token}'}

url = 'https://api.spotify.com/v1/me/tracks'

request_parameters = {'limit':'50'}

req = requests.get(url,params=request_parameters,headers=header)
response = req.json()

print(response.get('total'))


track_data = []
while True:
	total_records = response.get('total')

	for item in response['items']:
		row = [item.get('id'),
		item.get('name'),
		item.get('artists'),
		item.get('popularity'),
		item.get('duration_ms')]
		#print(row)
		track_data.append(row)

	if total_records <= len(track_data):
		break

	request_parameters = {'time_range':'long_term',
						'limit':'50',
						'offset':len(track_data)}

	req = requests.get(url,params=request_parameters,headers=header)
	#print(req.text)
	response = req.json()
	print(len(track_data))

print(len(track_data))